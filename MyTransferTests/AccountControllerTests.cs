﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoneyTransfer.Models;
using MoneyTransfer.Repositories;
using Moq;
using Xunit;

namespace MyTransferTests
{
    public class AccountControllerTests
    {
        [Fact]
        public void UniqueAccount()
        {
            var userRepMoq = new Mock<IUserRepository>();
            userRepMoq.Setup(repo => repo.GetUniqueAccount()).Returns(GetAccount());


            var result = userRepMoq.Object.GetUniqueAccount();
            User user = GetUserByAccount(result);

            var viewResult = Assert.IsType<string>(result);
            Assert.Equal(6, result.Length);
            Assert.Null(user);
        }

        private User GetUserByAccount(string account)
        {
            ApplicationContext context = new ApplicationContext(@"Server=(localdb)\mssqllocaldb;Database=MoneyTransferdb;Trusted_Connection=True;MultipleActiveResultSets=true");
            return context.Users.FirstOrDefault(u => u.Account == account);
        }
        private string GetAccount()
        {
            Random rnd = new Random();
            String account = rnd.Next(1, 999999).ToString("D6");
            if (GetUserByAccount(account) != null)
            {
                account = GetAccount();
            }

            return account;
        }
    }
}
