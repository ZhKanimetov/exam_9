﻿using System;
using System.Collections.Generic;
using System.Text;
using MoneyTransfer.Models;

namespace MyTransferTests
{
    public class ContextGetter
    {
        private static ApplicationContext context;

        public static ApplicationContext GetApplicationContext()
        {
            if (context == null)
            {
                context = new ApplicationContext(@"Server=(localdb)\mssqllocaldb;Database=MoneyTransferdbTest;Trusted_Connection=True;MultipleActiveResultSets=true");
            }

            return context;
        }
    }
}
