﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using MoneyTransfer.Models;
using MoneyTransfer.Repositories;
using Xunit;

namespace MyTransferTests
{
    public class UserControllerTests
    {
        private IUserRepository userRep = new UserRepository(ContextGetter.GetApplicationContext());
        private ITransferRepository transferRepository = new TransferRepository(ContextGetter.GetApplicationContext());

        [Fact]
        public async void CheckAccount()
        {
            IUserStore<User> userStore = new UserOnlyStore<User, ApplicationContext>(ContextGetter.GetApplicationContext());

            User user = new User() { Account = userRep.GetUniqueAccount() };

            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;

            await userStore.CreateAsync(user, token);

            User createdUser = userRep.FindByAccount(user.Account);

            Assert.Equal(user.Id, createdUser.Id);
            Assert.Equal(user.Account, createdUser.Account);

        }

        [Fact]
        public async void CheckBalance()
        {
            IUserStore<User> userStore = new UserOnlyStore<User, ApplicationContext>(ContextGetter.GetApplicationContext());

            User user = new User() { Account = userRep.GetUniqueAccount(), Balance = 100};
            User user2 = new User() { Account = userRep.GetUniqueAccount(), Balance = 100};

            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken token = source.Token;

            await userStore.CreateAsync(user, token);
            await userStore.CreateAsync(user2, token);

            userRep.RemoveBalance(user.Account, 200);
            userRep.RemoveBalance(user2.Account, 50);

            Assert.Equal(100, user.Balance);
            Assert.Equal(50, user2.Balance);
        }
    }
}
