﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneyTransfer.Models;

namespace MoneyTransfer.ViewModels
{
    public class TransferListViewModel
    {
        public List<Transfer> Transfers { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
