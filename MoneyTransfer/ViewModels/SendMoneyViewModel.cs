﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MoneyTransfer.ViewModels
{
    public class SendMoneyViewModel
    {
        [Required]
        [Remote(action: "CheckAccount", controller: "Validation")]
        public string Account { get; set; }

        [Required]
        [Remote(action: "CheckBalance", controller: "Validation")]
        [Range(1, 1000000, ErrorMessage = "Введите коректную сумму")]
        public double Balance { get; set; }

        [Required]
        [Range(1, 1000000, ErrorMessage = "Введите коректную сумму")]
        public double BalanceAnonymus { get; set; }
    }
}
