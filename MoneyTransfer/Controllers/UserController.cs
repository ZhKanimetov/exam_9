﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using MoneyTransfer.Models;
using MoneyTransfer.Repositories;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserRepository userRepository;
        private readonly ITransferRepository transferRepository;
        private readonly UserManager<User> userManager;

        public UserController(IUserRepository userRepository,
            ITransferRepository transferRepository,
            UserManager<User> userManager,
            IStringLocalizer<UserController> localizer)
        {
            this.userRepository = userRepository;
            this.transferRepository = transferRepository;
            this.userManager = userManager;
        }

        public IActionResult AddBalanceAnonymus()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddBalanceAnonymus(SendMoneyViewModel model)
        {
            userRepository.AddBalance(model.Account,model.BalanceAnonymus);
            return View();
        }

        public IActionResult SendMoney()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SendMoney(SendMoneyViewModel model)
        {
            User currentUser = await userManager.GetUserAsync(User);
            userRepository.AddBalance(model.Account, model.Balance);
            userRepository.RemoveBalance(currentUser.Account, model.Balance);
            transferRepository.AddEntity(new Transfer()
            {
                Balance = model.Balance,
                UserIdFrom = currentUser.Id,
                UserIdTo = userRepository.FindByAccount(model.Account).Id
            });

            return View();
        }

        public async Task<IActionResult> TransfersList(TransferListViewModel model)
        {
            User currentUser = await userManager.GetUserAsync(User);
            List<Transfer> transfers = transferRepository.FindUserTransfers(currentUser.Id);
            if (model.DateFrom != null)
            {
                transfers = transfers.Where(t => t.Date >= model.DateFrom).ToList();
            }

            if (model.DateTo != null)
            {
                transfers = transfers.Where(t => t.Date <= model.DateTo).ToList();
            }

            model = new TransferListViewModel()
            {
                Transfers = transfers,
                DateFrom = model.DateFrom,
                DateTo = model.DateTo
            };
            return View(model);
        }

        public string GetCulture(string code = "")
        {
            return $"CurrentCulture:{CultureInfo.CurrentCulture.Name}, CurrentUICulture:{CultureInfo.CurrentUICulture.Name}";
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }

    }
}