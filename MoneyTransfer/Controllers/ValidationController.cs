﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    public class ValidationController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly ApplicationContext context;

        public ValidationController(UserManager<User> userManager, ApplicationContext context)
        {
            this.userManager = userManager;
            this.context = context;
        }

        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> CheckAccount(string Account)
        {
            User user = context.Users.FirstOrDefault(u => u.Account == Account);
            User currentUser = await userManager.GetUserAsync(User);

            if (user == null)
            {
                return Json("Такой лицевой счет не найден !");
            }
            else if (user == currentUser)
            {
                return Json("Вы не можете перевести деньги сами себе  !");
            }

            return Json(true);
        }

        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> CheckBalance(double Balance)
        {
           User user = await userManager.GetUserAsync(this.User);

            if (user?.Balance < Balance)
            {
                return Json("У вас не хватает денег");
            }

            return Json(true);
        }
    }
}