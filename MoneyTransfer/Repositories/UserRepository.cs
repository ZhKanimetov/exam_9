﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MoneyTransfer.Models;

namespace MoneyTransfer.Repositories
{
    public interface IUserRepository : IRepositoryForUser<User>
    {
        string GetUniqueAccount();
        User FindByAccount(string account);
        void AddBalance(string account, double balance);
        void RemoveBalance(string account, double balance);
    }

    public class UserRepository : RepositoryForUser<User>, IUserRepository
    {

        public UserRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Users;
        }

        public string GetUniqueAccount()
        {
            String account = GetRandom.Get();
            if (FindByAccount(account) != null)
            {
                account = GetUniqueAccount();
            }

            return account;
        }

        public User FindByAccount(string account)
        {
            return AllEntities.FirstOrDefault(u => u.Account == account);
        }

        public void AddBalance(string account, double balance)
        {
            User user = FindByAccount(account);
            user.Balance += balance;
            AllEntities.Update(user);
            context.SaveChanges();
        }

        public void RemoveBalance(string account, double balance)
        {
            User user = FindByAccount(account);
            if (user.Balance >= balance)
            {
                user.Balance -= balance;
                AllEntities.Update(user);
                context.SaveChanges();
            }
        }

    }

    public class GetRandom
    {
        public static string Get()
        {
            Random rnd = new Random();
            return rnd.Next(1, 999999).ToString("D6");
        }
    }
}
