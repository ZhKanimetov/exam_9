﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Repositories
{
    public interface IRepositoryForUser<T> where T : IdentityUser
    {
    }

    public class RepositoryForUser<T> : IRepositoryForUser<T> where T : IdentityUser
    {
        protected readonly ApplicationContext context;
        protected DbSet<T> AllEntities { get; set; }

        public RepositoryForUser(ApplicationContext context)
        {
            this.context = context;
        }
    }
}
