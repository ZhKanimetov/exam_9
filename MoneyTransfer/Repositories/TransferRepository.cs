﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Repositories
{
    public interface ITransferRepository : IRepository<Transfer>
    {
        List<Transfer> FindUserTransfers(string id);
    }

    public class TransferRepository : Repository<Transfer>, ITransferRepository
    {
        public TransferRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Transfers;
        }

        public List<Transfer> FindUserTransfers(string id)
        {
            return AllEntities.Include(e => e.UserFrom).Include(e => e.UserTo).Where(e => e.UserIdTo == id || e.UserIdFrom == id).OrderBy(e => e.Date).ToList();
        }
    }
}
