﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        T FindById(int id);
        IEnumerable<T> GetAllEntities();
        void AddEntity(T entity);
    }

    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected readonly ApplicationContext context;
        protected DbSet<T> AllEntities { get; set; }

        public Repository(ApplicationContext context)
        {
            this.context = context;
        }

        public T FindById(int id)
        {
            try
            {
                return AllEntities.First(e => e.Id == id);
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
        }

        public IEnumerable<T> GetAllEntities()
        {
            return AllEntities.ToList();
        }

        public void AddEntity(T entity)
        {
            AllEntities.Add(entity);
            context.SaveChanges();
        }
    }
}
