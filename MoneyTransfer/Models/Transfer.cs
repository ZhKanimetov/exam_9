﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transfer : Entity
    {
        [ForeignKey("UserFrom")]
        public string UserIdFrom { get; set; }

        [ForeignKey("UserTo")]
        public string UserIdTo { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
        public double Balance { get; set; }

        public User UserFrom { get; set; }
        public User UserTo { get; set; }
    }
}
