﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MoneyTransfer.Models
{
    public class User : IdentityUser
    {
        public double Balance { get; set; }
        public string Account { get; set; }
    }
}
